# GiveUpGithub Migrator

Work in progress scripts to help with attaching protests to your existing repos.

## Things I wanna do

* [ ] Create destination repos from `gug.sh`
* [ ] Make it possible to configure `gug.sh` command with commandline flags
* [ ] Take care of tearing down repos to leave only the `give-up-github` branch created (dangerous!)
