#!/bin/sh
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
CODE_HOST="${CODE_HOST:-Gitlab}"
CODE_HOST_URL="${CODE_HOST_URL:-gitlab.com}"
DRY_RUN="${DRY_RUN:-y}"

prev_branch="$(git branch --show-current)"
current_upstream_url="$(git remote get-url origin)"
url=$(echo "${current_upstream_url}"|sed -e "s/github\.com/${CODE_HOST_URL}/g")
pub_url="$(echo "${url}"|sed -e "s#ssh://git@#https://#g")"

if [ ! -z "${DRY_RUN}" ];then
	echo "SCRIPT_DIR=${SCRIPT_DIR}"
	echo "CODE_HOST=${CODE_HOST}"
	echo "CODE_HOST_URL=${CODE_HOST_URL}"
	echo "prev_branch=${prev_branch}"
	echo "current_upstream_url=${current_upstream_url}"
	echo "url=${url}"
	echo "pub_url=${pub_url}"
fi

if [ -z "${DRY_RUN}" ];then
	git switch --orphan give-up-github
	cp "${SCRIPT_DIR}/GiveUpGithub-README.md" ./README.md
	git add README.md
	sed -i -e "s#\[INSERT-URL-OR-DESCRIPTION\]\(INSERT-URL\)#[${CODE_HOST}](${pub_url})#g"
	git commit -F "${SCRIPT_DIR}/commit-msg.txt"
	#git push -u origin give-up-github
	#git set-url origin "${url}"
	git checkout "${prev_branch}"
fi
